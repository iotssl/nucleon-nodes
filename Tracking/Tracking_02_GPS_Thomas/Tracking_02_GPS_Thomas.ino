/**************************************************************
 * 
 * Tracking Node der zum Tracken keine APP benötigt
 * 
 * Zusätzlich wird die Akku Spannung übertragen so das man 
 * damit einen Trigger auslösen kann um z.B. eine SMS oder 
 * Mail via IFTTT zu erhalten.
 * 
 * Gebaut für die Schulung des Nucleon e.V.
 * 
 * Hardware: 
 *  - Arduino Pro-Mini 3.3V 
 *  - RFM95W
 *  - GY-GPS6MV2
 *  
 * Software: 
 *  - LMIC https://github.com/matthijskooijman/arduino-lmic 
 *  - LowPower library https://github.com/rocketscream/Low-Power
 *  - special adcvcc library from Charles (see : https://www.thethingsnetwork.org/forum/t/full-arduino-mini-lorawan-and-1-3ua-sleep-mode/8059/32?u=lex_ph2lb )
 *  - TinyGPS++
 *  - SoftwareSerial
 *  
 * For licenses of the used libraries, check the links above.
 * *********************************************************
 * Stand: 11.11.2018
 * Version 0.1.0
 * by F Radzio
 ************************************************************/
/************************************************************
 * TheThingsNetwork Payload function:
function Decoder(b, port) 
  {
  var retValue =   { 
    bytes: b
    };
  if (port==1) {  
    retValue.batt = b[0] / 10.0;
    if (retValue.batt === 0)
     delete retValue.batt; 
    if (retValue.batt <= 2.8)
      retValue.trigger = true;
    else
      retValue.trigger = false;
   return retValue; 
  }
  if (port==2) {  
    retValue.batt = b[0] / 10.0;
    if (retValue.batt === 0)
     delete retValue.batt; 
    if (retValue.batt <= 2.8)
      retValue.trigger = true;
    else
      retValue.trigger = false;
      
    retValue.latitude = (b[1] | b[2]<<8 | b[3]<<16 | (b[3] & 0x80 ? 0xFF<<24 : 0)) / 10000;
    retValue.longitude = (b[4] | b[5]<<8 | b[6]<<16 | (b[6] & 0x80 ? 0xFF<<24 : 0)) / 10000;
    retValue.altitude = (b[7] | b[8]<<8 | (b[8] & 0x80 ? 0xFF<<16 : 0)) / 100;
    retValue.hdop = b[9] / 100;
  return retValue; 
  }
  }
 * 
 * 
 ***********************************************************/
 /****************************************************************************************
* INCLUDE FILES
****************************************************************************************/

// GPS
#include <TinyGPS++.h>
#include <SoftwareSerial.h>

// TTN

#include <lmic.h>
#include <hal/hal.h>
//#include <credentials.h>

// LowPower und Spannung messen
#include <LowPower.h>
#include  "adcvcc.h"  
/**********************************************************
 * Defines
 **********************************************************/
#define debugSerial Serial 
#define SHOW_DEBUGINFO  
#define debugPrintLn(...) { if (debugSerial) debugSerial.println(__VA_ARGS__); }
#define debugPrint(...) { if (debugSerial) debugSerial.print(__VA_ARGS__); } 

#define TRACKINGINTERVAL    10    // 10 seconds (for tracking) 
// im TRACKINGINTERVAL schaltet den Node aus wenn ihr nicht unterweg seid
// es soll das Netz ja nicht unnötig mit Daten geflutet werden
#define FASTINTERVAL    60        // 60 seconds (for testing)
#define NORMALINTERVAL  900        // (5)15 minutes (normal)
/************************************************************
 * GPS Definitionen
 ************************************************************/
static const int RXPin = 8, TXPin = 9; //GPS PIN
static const uint32_t GPSBaud = 9600;  //GPS Baud
uint8_t coords[10];  //Array für die Koordinaten

/********************************************************
 * Pin Mapping RFM95W zu Arduino
 ********************************************************/
#define LMIC_NSS    6
#define LMIC_RXTX   LMIC_UNUSED_PIN
#define LMIC_RST    5
#define LMIC_DIO0   2
#define LMIC_DIO1   3
#define LMIC_DIO2   4

//// Pin mapping Dragino Shield
//const lmic_pinmap lmic_pins = {
//    .nss = 10,
//    .rxtx = LMIC_UNUSED_PIN,
//    .rst = 9,
//    .dio = {2, 6, 7},
//};

const lmic_pinmap lmic_pins = {
    .nss = LMIC_NSS,
    .rxtx = LMIC_RXTX,   
    .rst = LMIC_RST,
    .dio = {LMIC_DIO0, LMIC_DIO1, LMIC_DIO2},  
}; 

/*****************************************************
 * TTN Config
 *****************************************************/
/***************************************************** 
 *  ABP 
 *****************************************************/
#ifdef CREDENTIALS
static const u1_t NWKSKEY[16] = NWKSKEY1;
static const u1_t APPSKEY[16] = APPSKEY1;
static const u4_t DEVADDR = DEVADDR1;
#else 
static const PROGMEM u1_t NWKSKEY[16] = { 0x6E, 0x40, 0x47, 0xA2, 0x8B, 0xFA, 0x7C, 0x8B, 0x36, 0x01, 0x9B, 0x5E, 0xF8, 0xDA, 0x2A, 0x0B }; // LoRaWAN NwkSKey, network session key 
static const u1_t PROGMEM APPSKEY[16] = { 0x15, 0xA9, 0xE9, 0x7D, 0x2E, 0x4A, 0xF6, 0x68, 0x58, 0xCC, 0x78, 0x4B, 0xD5, 0xC2, 0x6B, 0xBE }; // LoRaWAN AppSKey, application session key 
static const u4_t DEVADDR = 0x26011101 ; // LoRaWAN end-device address (DevAddr)
#endif
/****************************************************
 * OTAA
 * 
 * These callbacks are only used in over-the-air activation, so they are
 * left empty here (we cannot leave them out completely unless
 * DISABLE_JOIN is set in config.h, otherwise the linker will complain).
 ****************************************************/
void os_getArtEui (u1_t* buf) { }
void os_getDevEui (u1_t* buf) { }
void os_getDevKey (u1_t* buf) { }
/*************************************************** 
 * global enviromental parameters 
 ***************************************************/
static osjob_t sendjob;
/***************************************************************
 * Hier das Sende Intervall festlegen
 * TRACKINGINTERVAL alle 10 Sekunden
 * FASTINTERVAL alle 60 Sekunden
 * NORMALINTERVAL alle 900 Sekunden
 * 
 * Bei der Einstellung TRACKINGINTERVAL schaltet den Node bitte ab 
 * wenn ihr nich aktiv Tracken wollt.
 * Das Tracken belastet das Netwerk und die Airtime für andere Nodes
 */
int interval = TRACKINGINTERVAL;  

byte LMIC_transmitted = 0;
byte LMIC_event_Timeout = 0;

/***************************************************
 * TX Zeit die mindestens benötigt wird
 * Es kann durch Einschränkungen auch länger werden
 * Schedule TX every this many seconds (might become longer due to duty cycle limitations).
 * 
 * Diese Variable wird hier erst einmal nicht benötigt
 ***************************************************/
const unsigned TX_INTERVAL = TRACKINGINTERVAL;
/*****************************************************
 * The TinyGPS++ object
 *****************************************************/
TinyGPSPlus gps;

/*****************************************************
 *  The serial connection to the GPS device
 *****************************************************/
 SoftwareSerial ss(RXPin, TXPin);
/********************************************************************************
 * Funktionen
 ********************************************************************************/

/* ======================================================================
Function: ADC_vect
Purpose : IRQ Handler for ADC 
Input   : - 
Output  : - 
Comments: used for measuring 8 samples low power mode, ADC is then in 
          free running mode for 8 samples
====================================================================== */
ISR(ADC_vect)  
{
  // Increment ADC counter
  _adc_irq_cnt++;
}
/*******************************************************************************
 * Die Funktion updateEnvParameters dient dazu Sensoren auszulesen
 * Sie wird beim Senden aufgerufen
 *******************************************************************************/
void updateEnvParameters()
{
  debugPrintLn(F("GPS start"));  
  get_coords();
  debugPrintLn(F("GPS fertig")); 
} 
/*******************************************************************************
 * onEvent
 * Hier werden die Events der LMIC abgefangen um Aktionen daraus zu generieren
 * Zur Optimierung sind einige debugPrint Aufrufe auskommentiert das würde den 
 * Code nur unnötig aufblähen. Zum Debuging kann man es jederzeit wieder einschalten
 *******************************************************************************/
void onEvent (ev_t ev) 
{
    debugPrint(os_getTime());
    debugPrint(": ");
    debugPrintLn(ev);
    switch(ev) 
    {
        case EV_SCAN_TIMEOUT:
            //debugPrintLn(F("EV_SCAN_TIMEOUT"));
            break;
        case EV_BEACON_FOUND:
            //debugPrintLn(F("EV_BEACON_FOUND"));
            break;
        case EV_BEACON_MISSED:
            //debugPrintLn(F("EV_BEACON_MISSED"));
            break;
        case EV_BEACON_TRACKED:
            //debugPrintLn(F("EV_BEACON_TRACKED"));
            break;
        case EV_JOINING:
            //debugPrintLn(F("EV_JOINING"));
            break;
        case EV_JOINED:
            //debugPrintLn(F("EV_JOINED"));
            break;
        case EV_RFU1:
            //debugPrintLn(F("EV_RFU1"));
            break;
        case EV_JOIN_FAILED:
            //debugPrintLn(F("EV_JOIN_FAILED"));
            break;
        case EV_REJOIN_FAILED:
            //debugPrintLn(F("EV_REJOIN_FAILED"));
            break;
        case EV_TXCOMPLETE:
            debugPrintLn(F("EV_TXCOMPLETE"));
            if (LMIC.txrxFlags & TXRX_ACK)
              debugPrintLn(F("R ACK")); // Received ack
            if (LMIC.dataLen) 
            {
              debugPrintLn(F("R "));
              debugPrintLn(LMIC.dataLen);
              debugPrintLn(F(" bytes")); // of payload
            }            
            // Schedule next transmission
            // os_setTimedCallback(&sendjob, os_getTime()+sec2osticks(TX_INTERVAL), do_send);
            LMIC_transmitted = 1; 
            break;
        case EV_LOST_TSYNC:
            //debugPrintLn(F("EV_LOST_TSYNC"));
            break;
        case EV_RESET:
            //debugPrintLn(F("EV_RESET"));
            break;
        case EV_RXCOMPLETE:
            // data received in ping slot
            //debugPrintLn(F("EV_RXCOMPLETE"));
            break;
        case EV_LINK_DEAD:
            //debugPrintLn(F("EV_LINK_DEAD"));
            break;
        case EV_LINK_ALIVE:
            //debugPrintLn(F("EV_LINK_ALIVE"));
            break;
         default:
            //debugPrintLn(F("Unknown event"));
            break;
    }
}
/************************************************************************************
 * do_send
 * Hier werden nun die Daten versendet
 * Auch die Abfrage der Spannung ist an dieser Stelle eingebaut
 ************************************************************************************/
 // LMIC_setTxData2(1, (uint8_t*) coords, sizeof(coords), 0);
void do_send(osjob_t* j)
{
    // Check if there is not a current TX/RX job running
    if (LMIC.opmode & OP_TXRXPEND) 
    {
        debugPrintLn(F("OP_TXRXPEND")); //P_TXRXPEND, not sending
    } 
    else 
    {
        // Prepare upstream data transmission at the next possible time.
 
        // Here the sensor information should be retrieved 

        updateEnvParameters();
 
        int batt = (int)(readVcc() / 100);  // readVCC returns  mVolt need just 100mVolt steps
        byte batvalue = (byte)batt; // no problem putting it into a int. 
       
#ifdef SHOW_DEBUGINFO      
        debugPrint(F("B="));
        debugPrintLn(batt);
        debugPrint(F("BV="));
        debugPrintLn(batvalue);  
#endif

        unsigned char mydata[1];
        coords[0] = batvalue;      
        
        LMIC_setTxData2(2, coords, sizeof(coords), 0);
        debugPrintLn(F("PQ")); //Packet queued
    }
    // Next TX is scheduled after TX_COMPLETE event.
}

 /************************************************************************************+
  * Setup 
  * 
  * Hier werden die Parameter der LMIC eingestellt
  * Die Anmeldung des Node beim TTN
  * Die genutzte lokale Funk Frequenzen und die daraus resultierenden Kanäle
  * Aber auch die Datenrate und Sender Stärke (DR_SF7,14)
  * 
  */


void setup() {
    Serial.begin(115200);

  ss.begin(GPSBaud);

    // LMIC init
    os_init();
    // Reset the MAC state. Session and pending data transfers will be discarded.
    LMIC_reset();

    // Set static session parameters. Instead of dynamically establishing a session
    // by joining the network, precomputed session parameters are be provided.
    #ifdef PROGMEM
    // On AVR, these values are stored in flash and only copied to RAM
    // once. Copy them to a temporary buffer here, LMIC_setSession will
    // copy them into a buffer of its own again.
    uint8_t appskey[sizeof(APPSKEY)];
    uint8_t nwkskey[sizeof(NWKSKEY)];
    memcpy_P(appskey, APPSKEY, sizeof(APPSKEY));
    memcpy_P(nwkskey, NWKSKEY, sizeof(NWKSKEY));
    LMIC_setSession (0x1, DEVADDR, nwkskey, appskey);
    #else
    // If not running an AVR with PROGMEM, just use the arrays directly
    LMIC_setSession (0x1, DEVADDR, NWKSKEY, APPSKEY);
    #endif

    #if defined(CFG_eu868)
    // Set up the channels used by the Things Network, which corresponds
    // to the defaults of most gateways. Without this, only three base
    // channels from the LoRaWAN specification are used, which certainly
    // works, so it is good for debugging, but can overload those
    // frequencies, so be sure to configure the full frequency range of
    // your network here (unless your network autoconfigures them).
    // Setting up channels should happen after LMIC_setSession, as that
    // configures the minimal channel set.
    // NA-US channels 0-71 are configured automatically
    LMIC_setupChannel(0, 868100000, DR_RANGE_MAP(DR_SF12, DR_SF7),  BAND_CENTI);      // g-band
    LMIC_setupChannel(1, 868300000, DR_RANGE_MAP(DR_SF12, DR_SF7B), BAND_CENTI);      // g-band
    LMIC_setupChannel(2, 868500000, DR_RANGE_MAP(DR_SF12, DR_SF7),  BAND_CENTI);      // g-band
    LMIC_setupChannel(3, 867100000, DR_RANGE_MAP(DR_SF12, DR_SF7),  BAND_CENTI);      // g-band
    LMIC_setupChannel(4, 867300000, DR_RANGE_MAP(DR_SF12, DR_SF7),  BAND_CENTI);      // g-band
    LMIC_setupChannel(5, 867500000, DR_RANGE_MAP(DR_SF12, DR_SF7),  BAND_CENTI);      // g-band
    LMIC_setupChannel(6, 867700000, DR_RANGE_MAP(DR_SF12, DR_SF7),  BAND_CENTI);      // g-band
    LMIC_setupChannel(7, 867900000, DR_RANGE_MAP(DR_SF12, DR_SF7),  BAND_CENTI);      // g-band
    LMIC_setupChannel(8, 868800000, DR_RANGE_MAP(DR_FSK,  DR_FSK),  BAND_MILLI);      // g2-band


    // For single channel gateways: Restrict to channel 0 when defined above
#ifdef CHANNEL0
    LMIC_disableChannel(1);
    LMIC_disableChannel(2);
    LMIC_disableChannel(3);
    LMIC_disableChannel(4);
    LMIC_disableChannel(5);
    LMIC_disableChannel(6);
    LMIC_disableChannel(7);
    LMIC_disableChannel(8);
#endif

    // TTN defines an additional channel at 869.525Mhz using SF9 for class B
    // devices' ping slots. LMIC does not have an easy way to define set this
    // frequency and support for class B is spotty and untested, so this
    // frequency is not configured here.
    #elif defined(CFG_us915)
    // NA-US channels 0-71 are configured automatically
    // but only one group of 8 should (a subband) should be active
    // TTN recommends the second sub band, 1 in a zero based count.
    // https://github.com/TheThingsNetwork/gateway-conf/blob/master/US-global_conf.json
    LMIC_selectSubBand(1);
    #endif

    // Disable link check validation
    LMIC_setLinkCheckMode(0);

    // TTN uses SF9 for its RX2 window.
    LMIC.dn2Dr = DR_SF9;

    // Set data rate and transmit power for uplink (note: txpow seems to be ignored by the library)
    LMIC_setDrTxpow(DR_SF7,14); 
    debugPrintLn(F("S")); // Setup complete!"
//    delay(1000);  // allow serial to send.
}

static void printInt(unsigned long val, bool valid, int len)
{
  char sz[32] = "*****************";
  if (valid)
    sprintf(sz, "%ld", val);
  sz[len] = 0;
  for (int i=strlen(sz); i<len; ++i)
    sz[i] = ' ';
  if (len > 0) 
    sz[len-1] = ' ';
  Serial.print(sz);
  smartDelay(0);
}

static void printDateTime(TinyGPSDate &d, TinyGPSTime &t)
{
  if (!d.isValid())
  {
    Serial.print(F("********** "));
  }
  else
  {
    char sz[32];
    sprintf(sz, "%02d/%02d/%02d ", d.month(), d.day(), d.year());
    Serial.print(sz);
  }
  
  if (!t.isValid())
  {
    Serial.print(F("******** "));
  }
  else
  {
    char sz[32];
    sprintf(sz, "%02d:%02d:%02d ", t.hour(), t.minute(), t.second());
    Serial.print(sz);
  }

  printInt(d.age(), d.isValid(), 5);
  smartDelay(0);
}

void get_coords () {
//  bool newData = false;
  unsigned long chars;
  unsigned short sentences, failed;
  float flat, flon, faltitudeGPS, fhdopGPS;
  unsigned long age;

     smartDelay(1000);
Serial.println("Get Data");
  flat = gps.location.lat();
Serial.println(flat);
  flon = gps.location.lng();
Serial.println(flon);
  faltitudeGPS = gps.altitude.meters();
Serial.println(faltitudeGPS);
  fhdopGPS = gps.hdop.hdop();
Serial.println(fhdopGPS);
printDateTime(gps.date, gps.time);
//  gps.stats(&chars, &sentences, &failed);

  int32_t lat = flat * 10000;
  int32_t lon = flon * 10000;
  int16_t altitudeGPS = faltitudeGPS * 100;
  int8_t hdopGPS = fhdopGPS; 

  // Pad 2 int32_t to 6 8uint_t, big endian (24 bit each, having 11 meter precision)
  coords[1] = lat;
  coords[2] = lat >> 8;
  coords[3] = lat >> 16;

  coords[4] = lon;
  coords[5] = lon >> 8;
  coords[6] = lon >> 16;
  
  coords[7] = altitudeGPS;
  coords[8] = altitudeGPS >> 8;

  coords[9] = hdopGPS;
}

// This custom version of delay() ensures that the gps object
// is being "fed".
static void smartDelay(unsigned long ms)
{
  unsigned long start = millis();
  do 
  {
    while (ss.available())
      gps.encode(ss.read());
  } while (millis() - start < ms);
}

/**************************************************************************************
 * loop
 * Hier nun der Loop, also das Hauptprogramm
 * 
 */

void loop() 
{ 
    // Start job
    do_send(&sendjob);
    // Wait for response of the queued message (check if message is send correctly)
    os_runloop_once();
    // Continue until message is transmitted correctly
    //debugPrintLn("\tWaiting for transmittion\n"); 
    debugPrintLn(F("W\n"));
    while(LMIC_transmitted != 1) 
    {
        os_runloop_once();
        // Add timeout counter when nothing happens:
        LMIC_event_Timeout++;
        delay(1000);
        if (LMIC_event_Timeout >= 60) 
        {
            // Timeout when there's no "EV_TXCOMPLETE" event after 60 seconds
            debugPrintLn(F("\tETimeout, msg not tx\n"));
            break;
        } 
    }

    LMIC_transmitted = 0;
    LMIC_event_Timeout = 0;
    //debugPrintLn(F("Going to sleep."));
    //delay(1000);  // allow serial to send.
/********************************************************************************    
 *     Hier wird der Node in den Schlafmodus gelegt
 *******************************************************************************/
 if (interval > TRACKINGINTERVAL)
   { 
    for (int i = 0; i < interval; i++)
    {  
          i +=7 ; // no normal 1 second run but 8 second loops m.      
          // Enter power down state for 8 s with ADC and BOD module disabled
          LowPower.powerDown(SLEEP_8S, ADC_OFF, BOD_OFF);   
    }
   }   
    //debugPrintLn("---");
}
