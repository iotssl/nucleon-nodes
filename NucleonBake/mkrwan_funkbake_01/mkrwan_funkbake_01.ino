#include <MKRWAN.h>
#include <RTCZero.h>
#include "arduino_secrets.h" 

// Select your region (AS923, AU915, EU868, KR920, IN865, US915, US915_HYBRID)
_lora_band region = EU868;


/* Create an rtc object */
RTCZero rtc;

/* Change these values to set the current initial time */
const byte seconds = 0;
const byte minutes = 0;
const byte hours = 0;

/* Change these values to set the current initial date */
const byte day = 12;
const byte month = 07;
const byte year = 19;


/********************************************************
 * Datenreate und Spreadfaktoren festlegen
 ********************************************************/

static int oldisf = 0;
static int isf = 0;
static int i = 0;

LoRaModem modem(Serial1);

void setup() {
  Serial.begin(115200);
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, LOW); 
        digitalWrite(LED_BUILTIN, HIGH);
        delay(100);  
        digitalWrite(LED_BUILTIN, LOW); 
        delay(100);  
        digitalWrite(LED_BUILTIN, HIGH);
        delay(100);  
        digitalWrite(LED_BUILTIN, LOW); 
        delay(100);  
        digitalWrite(LED_BUILTIN, HIGH);
        delay(100);  
        digitalWrite(LED_BUILTIN, LOW); 
        delay(100);  
        digitalWrite(LED_BUILTIN, HIGH);
        delay(100);  
        digitalWrite(LED_BUILTIN, LOW); 
        delay(100);  

//  while (!Serial);
  if (!modem.begin(region)) {
    Serial.println("Failed to start module");
    while (1) {
        digitalWrite(LED_BUILTIN, HIGH);
        delay(50);  
        digitalWrite(LED_BUILTIN, LOW);
      }
  };
  Serial.print("Your device EUI is: ");
  Serial.println(modem.deviceEUI());

  int connected = modem.joinOTAA(appEui, appKey);
//  if (!connected) {
//    Serial.println("Something went wrong; are you indoor? Move near a window and retry");
//    while (1) {}
//  }
//  Serial.println("Successfully joined the network!");

//  Serial.println("Enabling ADR and setting low spreading factor");
//  modem.setADR(true);
  modem.dataRate(0); //SF12

// Set RTC


  rtc.begin();

  rtc.setTime(hours, minutes, seconds);
  rtc.setDate(day, month, year);

  rtc.setAlarmTime(01, 00, 00);
  rtc.enableAlarm(rtc.MATCH_HHMMSS);

  rtc.attachInterrupt(alarmMatch);


  Serial.print(rtc.getHours());
  Serial.print(":");  
  Serial.print(rtc.getMinutes());
  Serial.print(":"); 
  Serial.println(rtc.getSeconds());

}

void set_sf() {

/*
  DataRate  Modulation  SF  BW  bit/s
  0   LoRa  12  125   250
  1   LoRa  11  125   440
  2   LoRa  10  125   980
  3   LoRa  9   125   1'760
  4   LoRa  8   125   3'125
  5   LoRa  7   125   5'470
  6   LoRa  7   250   11'000
*/
  oldisf = isf;
  Serial.print("oldisf: ");
  Serial.println(oldisf);    
  switch (oldisf) {
    case 0: modem.dataRate(1);
            isf = 1;
            Serial.print("isf: ");
            Serial.println(isf); 
            Serial.println("SF11");                
            break;      
    case 1: modem.dataRate(2);
            isf = 2;
            Serial.print("isf: ");
            Serial.println(isf);    
            Serial.println("SF10");
            break;      
    case 2: modem.dataRate(3);
            isf = 3;
            Serial.print("isf: ");
            Serial.println(isf);    
            Serial.println("SF9");
            break;      
    case 3: modem.dataRate(4);
            isf = 4;
            Serial.print("isf: ");
            Serial.println(isf);    
            Serial.println("SF8");
            break;      
    case 4: modem.dataRate(5);
            isf = 5;
            Serial.print("isf: ");
            Serial.println(isf);    
            Serial.println("SF7");
            break;      
    case 5: modem.dataRate(0);
            isf = 0;
            Serial.print("isf: ");
            Serial.println(isf);    
            Serial.println("SF12");
            break;      
//    case 6: modem.dataRate(0);
//            isf = 0;
//            Serial.print("isf: ");
//            Serial.println(isf);    
//            break;   
  }
 delay(1000);  
}

void alarmMatch()
{
  digitalWrite(LED_BUILTIN, HIGH);
}

void sleep1h() {
  rtc.setTime(hours, minutes, seconds);
  rtc.setDate(day, month, year);
  rtc.setAlarmTime(01, 00, 00);
  digitalWrite(LED_BUILTIN, LOW);  
  rtc.standbyMode();
}

void sleep1m() {

  Serial.print(rtc.getHours());
  Serial.print(":");  
  Serial.print(rtc.getMinutes());
  Serial.print(":"); 
  Serial.println(rtc.getSeconds());
      
  rtc.setTime(hours, minutes, seconds);
  rtc.setDate(day, month, year);
  rtc.setAlarmTime(00, 01, 00);
  digitalWrite(LED_BUILTIN, LOW);
  rtc.standbyMode();
}

void loop() {
  modem.beginPacket();
  digitalWrite(LED_BUILTIN, HIGH);
 i = 0;
 while (i<=5) {
  Serial.print(rtc.getHours());
  Serial.print(":");  
  Serial.print(rtc.getMinutes());
  Serial.print(":"); 
  Serial.println(rtc.getSeconds());

  modem.print(isf);
  int err = modem.endPacket(false);
  Serial.print(isf);
  if (err > 0) {
    Serial.println(" success!");
        digitalWrite(LED_BUILTIN, HIGH);
        delay(100);  
        digitalWrite(LED_BUILTIN, LOW); 
        delay(100);  
        digitalWrite(LED_BUILTIN, HIGH);   
  } else {
    Serial.println(" Error");
        digitalWrite(LED_BUILTIN, HIGH);
        delay(50);  
        digitalWrite(LED_BUILTIN, LOW); 
        delay(50);  
        digitalWrite(LED_BUILTIN, HIGH);
        delay(50);  
        digitalWrite(LED_BUILTIN, LOW); 
        delay(50);  
        digitalWrite(LED_BUILTIN, HIGH);    
  }
  delay(1000*60);
//  sleep1m();
  i++;
  set_sf();
 }
 Serial.println("Pause!");
// delay(10000*60); 
 sleep1h();
}
