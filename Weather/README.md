# Nucleon Nodes

# Anpassungen
29.04.2019 die Nodes können nun auch mittels OTAA betrieben werden.
           Der Sleep Mode ist angepasst worden.
           Zur weiteren Optimierung kann der Spannungswandler ausgebaut werden, dann sollte der BaseNode nur noch 0,008 mA in der Ruhephase verbrauchen.



Der Nucleon Wethernode

Alle Nodes, die Temperaturen, Luftdruck und/oder Luftfeuchte messen, werden unter Wetternodes zusammen gefasst.
Da es eine große Anzahl von verschiedenen Sensoren gibt, haben wir uns entschieden, dass die Sensortypen in eigenen Ports abgebildet werden sollten.

## Port 1

Sensor: BME280 (BME180)
Messwerte

 * Temperatur
 * Luftdruck


Beschreibung:

Der BME180 ist ein I2C Sensor, der ohne Probleme auch in den Schlafmodus zu versetzen ist.
Für Arduino gibt es ein BME280I2C.h Lib

Git Link zum Repo:

## Port 2

Sensor: BMP280
Messwerte

 * Temperatur
 * Luftdruck
 * Luftfeuchte

Beschreibung:

Der BMP280 ist ein I2C Sensor, der ohne Probleme auch in den Schlafmodus zu versetzen ist.
Für Arduino gibt es ein Adafruit_BMP280.h Lib


Git Link zum Repo:

## Port 3

Sensor: BME680
Messwerte

 * Temperatur
 * Luftdruck
 * Luftfeuchte
 * VOC Rohwert
 * VOC Index

Beschreibung:

Der BME680 ist ein I2C Sensor, der ohne Probleme auch in den Schlafmodus zu versetzen ist.
Für Arduino gibt es ein BlueDot_BME680.h Lib
Der Luftqualitätsindex ist etwas umständlich zu berechenen, da als Signal ein Widerstandswert ausgegeben wird, der bei steigender Konzentration sinkt.

> Messung der VOC-Konzentration. Sie können hohe Konzentrationen von VOC in der Umgebungsluft feststellen. Das Rohsignal ist ein Widerstandswert in Ohm, der mit steigender VOC-Konzentration sinkt.

Git Link zum Repo:

## Port 4

Sensor: DHT11 / 21 / 22 / 33 / 44
Spezifikationen

### DHT 11
* Link: https://shop.boxtec.ch/digital-humiditytemperature-sensor-dht11-rht01-p-40242.html
* Temperatur 0 - 50°C
* Luftfeuchte 20-90%
### DHT 21
* Link: 
* Temperatur 0 - 50°C
* Luftfeuchte 20-80%
### DHT 22
* Link: https://shop.boxtec.ch/digital-humiditytemperature-sensor-dht22-rht03-p-40371.html
* Temperatur -40 - 80°C
* Luftfeuchte 0-100%
### DHT 33
* Link: https://shop.boxtec.ch/digital-humiditytemperature-sensor-dht33-rht04-p-40541.html
* Temperatur -40 - 100°C
* Luftfeuchte 0-100%
### DHT 44 
* Link: https://shop.boxtec.ch/digital-humidity-temperature-sensor-dht44-rht05-p-40846.html
* Temperatur -40 - 100°C
* Luftfeuchte 0-100%

Messwerte

 * Temperatur
 * Luftfeuchte

Beschreibung:

Der DHT11 ist ein 1-wire Sensor, der eine eigene Lib benötigt
Für Arduino gibt es 2 Libs, die zur Ansteuerung benötigt werden, DHT_U.h und DHT.h
Der Sleep Modus muss durch eine geeignete Beschaltung die Spannungsversorgung des Sensors unterbrechen.
Die DHT Sensoren benötigen relativ viel Strom (40mA) und können maximal alle 2 Sekunden ausgelesen werden.

Git Link zum Repo:

## Port 5

Sensor: DS18B20
Messwerte

 * Temperatur -40 - 100°C

Beschreibung:

Der DS18B20 ist ein 1-wire Sendor, der wie üblich keinen Schlafmodus kennt und entsprechend beschaltet werden muss.
Die benötigten Libs sind OneWire.h und DallasTemperature.h

## Port 6

Hier ist noch Grundlagenforschung notwendig
Dieser Sensor scheint sehr interessant zu sein!
Sensor: SHT 20 / 21 / 25 so wie 30 / 31 / 35

Die SHT Serie sind I2C Sensoren, die sehr wenig Strom in ihrerm Schlafmodus benötigen, allerdings im Luftfeuchtebereich eine sehr lange Ansprechzeit aufweisen um genaue Werte zu liefern.
Weitere Infos https://www.sensirion.com/de/umweltsensoren/feuchtesensoren/feuchte-temperatursensor-sht2x-digital-i2c-genauigkeit/
Messwerte

 * Temperatur -40 to +125°C
 * Luftfeuchte 0-100%

Beschreibung:

Die Sensoren der SHT 2x Serie benötigen neben der wire.h auch noch die SHT2x.h ,die es von unterschiedlichen Herstellern gibt.
Bei den etwas hochwertigeren SHT 3x Serie gibt es sogar noch Versionen, die mit Filtermembran ausgestattet sind und somit den IP67 Standard unterstützen. Hier gibt es als lib die SHT3x.h von unterschiedlichen Herstellern.

## Port 7

Hier ist noch Grundlagenforschung notwendig
Sensor:
Messwerte

 * Windrichtung

## Port 8

Hier ist noch Grundlagenforschung notwendig
Sensor:
Messwerte

 * Windgeschwindigkeit

## Port 9

Hier ist noch Grundlagenforschung notwendig
Sensor:
Messwerte

 * Regenmenge

## Port 10

Hier ist noch Grundlagenforschung notwendig
Sensor:
Messwerte

 * UV

## Port 11

Hier ist noch Grundlagenforschung notwendig
Sensor:
Messwerte

 * LUX
