/**************************************************************
 * 
 * Weather Node 02 
 * 
 * Sensortyp BME280
 * 
 * Zusätzlich wird die Akku Spannung übertragen so das man 
 * damit einen Trigger auslösen kann um z.B. eine SMS oder 
 * Mail via IFTTT zu erhalten.
 * 
 * Gebaut für die Schulung des Nucleon e.V.
 * 
 * Hardware: 
 *  - Arduino Pro-Mini 3.3V 
 *  - RFM95W
 *  - BME280 sensor.
 *  
 * Software: 
 *  - LMIC https://github.com/matthijskooijman/arduino-lmic 
 *  - LowPower library https://github.com/rocketscream/Low-Power
 *  - special adcvcc library from Charles (see : https://www.thethingsnetwork.org/forum/t/full-arduino-mini-lorawan-and-1-3ua-sleep-mode/8059/32?u=lex_ph2lb )
 *  
 * For licenses of the used libraries, check the links above.
 * *********************************************************
 * Stand: 11.11.2018
 * Version 0.1.0
 * by F Radzio
 ************************************************************/

/************************************************************
 * TheThingsNetwork Payload function:
    function Decoder(bytes, port) 
    {
      var retValue =   { 
        bytes: bytes
      };
      
      retValue.batt = bytes[0] / 10.0;
      if (retValue.batt === 0)
         delete retValue.batt; 
     
      if (bytes.length >= 2)
      {
        retValue.humidity = bytes[1];
        if (retValue.humidity === 0)
          delete retValue.humidity; 
      } 
      if (bytes.length >= 3)
      {
        retValue.temperature = (((bytes[2] << 8) | bytes[3]) / 10.0) - 40.0;
      } 
      if (bytes.length >= 5)
      { 
        retValue.pressure = ((bytes[4] << 8) | bytes[5]); 
        if (retValue.pressure === 0)
          delete retValue.pressure; 
      }
       
      return retValue; 
    }
 * 
 * 
 ***********************************************************/
;  //this line ALSO solves everything!
/****************************************************************************************
* INCLUDE FILES
****************************************************************************************/

#include <lmic.h>
#include <hal/hal.h>
#include <LowPower.h>
#include  "adcvcc.h"
/**********************************************************  
 *   Notwendige Libs für den BME280
 **********************************************************/
#include <Wire.h>   
#include  "BME280I2C.h"
/**********************************************************
 * Defines
 **********************************************************/

#define debugSerial Serial 
#define SHOW_DEBUGINFO  
#define debugPrintLn(...) { if (debugSerial) debugSerial.println(__VA_ARGS__); }
#define debugPrint(...) { if (debugSerial) debugSerial.print(__VA_ARGS__); } 

#define TRACKINGINTERVAL    10    // 10 seconds (for tracking) 
// im TRACKINGINTERVAL schaltet den Node aus wenn ihr nicht unterweg seid
// es soll das Netz ja nicht unnötig mit Daten geflutet werden
#define FASTINTERVAL    60        // 60 seconds (for testing)
#define NORMALINTERVAL  900       // (5)15 minutes (normal)

/********************************************************
 * Pin Mapping RFM95W zu Arduino
 ********************************************************/
#define LMIC_NSS    6
#define LMIC_RXTX   LMIC_UNUSED_PIN
#define LMIC_RST    5
#define LMIC_DIO0   2
#define LMIC_DIO1   3
#define LMIC_DIO2   4


const lmic_pinmap lmic_pins = {
    .nss = LMIC_NSS,
    .rxtx = LMIC_RXTX,   
    .rst = LMIC_RST,
    .dio = {LMIC_DIO0, LMIC_DIO1, LMIC_DIO2},  
}; 

/*****************************************************
 * TTN Config
 *****************************************************/
#define OTAA 1
#if !defined(OTAA)   
/***************************************************** 
 *  ABP 
 *****************************************************/
static const PROGMEM u1_t NWKSKEY[16] = {  }; // LoRaWAN NwkSKey, network session key 
static const u1_t PROGMEM APPSKEY[16] = {  }; // LoRaWAN AppSKey, application session key 
static const u4_t DEVADDR = 0x000000 ; // LoRaWAN end-device address (DevAddr)
/****************************************************
 * OTAA
 * 
 * Diese callbacks werden nur für die over-the-air activation verwendet
 * hier leer gelassen (wir können sie nicht vollständig auslassen, es sei denn
 * DISABLE_JOIN ist in config.h eingestellt, sonst beschwert sich der Linker).
 ****************************************************/
void os_getArtEui (u1_t* buf) { }
void os_getDevEui (u1_t* buf) { }
void os_getDevKey (u1_t* buf) { }
/*********************************************************/
#else
/*******************************************************************************
 * OTAA Teil
 *******************************************************************************/
/*******************************************************************************
 * Diese EUI muss im Little-Endian-Format vorliegen, also das niederwertigste Byte
 * zuerst. Beim Kopieren eines EUI aus der Ausgabe von ttnctl bedeutet dies das Umkehren
 * die Bytes Für von TTN ausgegebene EUIs sollten die letzten Bytes 0xD5, 0xB3 sein.
 * 0x70.
 *******************************************************************************/

static const u1_t PROGMEM APPEUI[8]={ 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
void os_getArtEui (u1_t* buf) { memcpy_P(buf, APPEUI, 8);}
/*******************************************************************************
 * Dies sollte auch im Little-Endian-Format erfolgen, siehe oben.
 *******************************************************************************/
static const u1_t PROGMEM DEVEUI[8]={ 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
void os_getDevEui (u1_t* buf) { memcpy_P(buf, DEVEUI, 8);}
/*******************************************************************************
 * Dieser Schlüssel sollte im Big Endian-Format vorliegen (oder, da er nicht wirklich ein
 * Nummer, aber ein Speicherblock, Endianness trifft nicht wirklich zu. Im
 * In der Praxis kann ein aus ttnctl entnommener Schlüssel so wie er ist kopiert werden.
 *******************************************************************************/
static const u1_t PROGMEM APPKEY[16] = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
void os_getDevKey (u1_t* buf) {  memcpy_P(buf, APPKEY, 16);}
#endif
/*************************************************** 
 * globale Umgebungsparameter
 ***************************************************/
static osjob_t sendjob; 
/***************************************************
 * Die Variablen für den BME280
 ***************************************************/
static float temp = 0.0;
static float pressure = 0.0;
static float humidity = 0.0;    
/**************************************************
 * Das bme Objekt
 * Default : forced mode, standby time = 1000 ms
 * Oversampling = pressure ×1, temperature ×1, humidity ×1, filter off,
 */
BME280I2C bme;   

/***************************************************************
 * Hier das Sende Intervall festlegen
 * TRACKINGINTERVAL alle 10 Sekunden
 * FASTINTERVAL alle 60 Sekunden
 * NORMALINTERVAL alle 900 Sekunden
 * 
 * Bei der Einstellung TRACKINGINTERVAL schaltet den Node bitte ab 
 * wenn ihr nich aktiv Tracken wollt.
 * Das Tracken belastet das Netwerk und die Airtime für andere Nodes
 */
int interval = NORMALINTERVAL;  

byte LMIC_transmitted = 0;
byte LMIC_event_Timeout = 0;

/***************************************************
 * TX Zeit die mindestens benötigt wird
 * Es kann durch Einschränkungen auch länger werden
 * Schedule TX every this many seconds (might become longer due to duty cycle limitations).
 * 
 * Diese Variable wird hier erst einmal nicht benötigt
 ***************************************************/
 
const unsigned TX_INTERVAL = NORMALINTERVAL;

/********************************************************************************
 * Variable für die empfangenen Daten 
 * 
 *  * Diese Variable wird hier erst einmal nicht benötigt
 ********************************************************************************/
 //unsigned char myrecdata[7];
 
/********************************************************************************
 * Funktionen
 ********************************************************************************/

/* ======================================================================
Function: ADC_vect
Purpose : IRQ Handler for ADC 
Input   : - 
Output  : - 
Comments: Der ADC ist für die Messung von 8 Abtastwerten im Low-Power-Modus vorgesehen
          Freilaufmodus für 8 Samples
====================================================================== */
ISR(ADC_vect)  
{
  // ADC-Zähler inkrementieren
  _adc_irq_cnt++;
}
/*******************************************************************************
 * Die Funktion updateEnvParameters dient dazu Sensoren auszulesen
 * Sie wird beim Senden aufgerufen
 *******************************************************************************/
void updateEnvParameters()
{
/***********************************************************************************
 * Die drei Variablen für Temperatur, Luftdruck und Luftfeuchtigkeit werden befüllt
 * Aufruf vor jedem Senden damit die Werte auch aktuell sind
 ***********************************************************************************/

   BME280::TempUnit tempUnit(BME280::TempUnit_Celsius);
   BME280::PresUnit presUnit(BME280::PresUnit_hPa);
 
    temp = bme.temp(tempUnit);
    pressure = bme.pres(presUnit);    // 1 = hPa (milliBar)  
    humidity =  bme.hum();
     

} 
/*******************************************************************************
 * sleepmode
 * Lege den Node Schlafen
 *******************************************************************************/

void sleepmode()
{
    debugPrint(os_getTime());
    debugPrintLn(" sleep on ");
delay(500);
 if (interval > TRACKINGINTERVAL)
   { 
    for (int i = 0; i < interval; i++)
    {  
          i +=7 ; // no normal 1 second run but 8 second loops m. 
          // Enter power down state for 8 s with ADC and BOD module disabled
          LowPower.powerDown(SLEEP_8S, ADC_OFF, BOD_OFF);   
    }
   } 
    debugPrint(os_getTime());
    debugPrintLn(" sleep off ");
       
}
/*******************************************************************************
 * onEvent
 * Hier werden die Events der LMIC abgefangen um Aktionen daraus zu generieren
 * Zur Optimierung sind einige debugPrint Aufrufe auskommentiert das würde den 
 * Code nur unnötig aufblähen. Zum Debuging kann man es jederzeit wieder einschalten
 *******************************************************************************/
void onEvent (ev_t ev) 
{
    debugPrint(os_getTime());
    debugPrint(": ");
    debugPrintLn(ev);
    switch(ev) 
    {
        case EV_SCAN_TIMEOUT:
            //debugPrintLn(F("EV_SCAN_TIMEOUT"));
            break;
        case EV_BEACON_FOUND:
            //debugPrintLn(F("EV_BEACON_FOUND"));
            break;
        case EV_BEACON_MISSED:
            //debugPrintLn(F("EV_BEACON_MISSED"));
            break;
        case EV_BEACON_TRACKED:
            //debugPrintLn(F("EV_BEACON_TRACKED"));
            break;
        case EV_JOINING:
            //debugPrintLn(F("EV_JOINING"));
            break;
        case EV_JOINED:
            //debugPrintLn(F("EV_JOINED"));
            break;
        case EV_RFU1:
            //debugPrintLn(F("EV_RFU1"));
            break;
        case EV_JOIN_FAILED:
            //debugPrintLn(F("EV_JOIN_FAILED"));
            break;
        case EV_REJOIN_FAILED:
            //debugPrintLn(F("EV_REJOIN_FAILED"));
            break;
        case EV_TXCOMPLETE:
            debugPrintLn(F("EV_TXCOMPLETE"));
            if (LMIC.txrxFlags & TXRX_ACK)
              debugPrintLn(F("R ACK")); // Received ack
            if (LMIC.dataLen) 
            {
              debugPrintLn(F("R "));
              debugPrintLn(LMIC.dataLen);
              debugPrintLn(F(" bytes")); // of payload
            }            
            // Schedule next transmission
            // os_setTimedCallback(&sendjob, os_getTime()+sec2osticks(TX_INTERVAL), do_send);
      for (int i=0; i<int(TX_INTERVAL/8); i++) {
        // Use library from https://github.com/rocketscream/Low-Power
        LowPower.powerDown(SLEEP_8S, ADC_OFF, BOD_OFF);
      }
      do_send(&sendjob);            
            LMIC_transmitted = 1; 
            break;
        case EV_LOST_TSYNC:
            //debugPrintLn(F("EV_LOST_TSYNC"));
            break;
        case EV_RESET:
            //debugPrintLn(F("EV_RESET"));
            break;
        case EV_RXCOMPLETE:
            // data received in ping slot
            //debugPrintLn(F("EV_RXCOMPLETE"));
            break;
        case EV_LINK_DEAD:
            //debugPrintLn(F("EV_LINK_DEAD"));
            break;
        case EV_LINK_ALIVE:
            //debugPrintLn(F("EV_LINK_ALIVE"));
            break;
         default:
            //debugPrintLn(F("Unknown event"));
            break;
    }
}
/************************************************************************************
 * do_send
 * Hier werden nun die Daten versendet
 * Auch die Abfrage der Spannung ist an dieser Stelle eingebaut
 ************************************************************************************/
void do_send(osjob_t* j)
{
    // Check if there is not a current TX/RX job running
    if (LMIC.opmode & OP_TXRXPEND) 
    {
        debugPrintLn(F("OP_TXRXPEND")); //P_TXRXPEND, not sending
    } 
    else 
    {
        // Prepare upstream data transmission at the next possible time.
 
        // Here the sensor information should be retrieved 
        //  Pressure: 300...1100 hPa
        //  Temperature: -40…85°C  

        updateEnvParameters();
 
        int batt = (int)(readVcc() / 100);  // readVCC returns  mVolt need just 100mVolt steps
        byte batvalue = (byte)batt; // no problem putting it into a int. 
       
#ifdef SHOW_DEBUGINFO
        debugPrint(F("T="));
        debugPrintLn(temp); 
        
        debugPrint(F("P="));
        debugPrintLn(pressure); 
        
        debugPrint(F("H="));
        debugPrintLn(humidity); 
      
        debugPrint(F("B="));
        debugPrintLn(batt);
        debugPrint(F("BV="));
        debugPrintLn(batvalue);  
#endif
        int t = (int)((temp + 40.0) * 10.0); 
        // t = t + 40; => t [-40..+85] => [0..125] => t = t * 10; => t [0..125] => [0..1250]
        int p = (int)(pressure);  // p [300..1100]
        int h = (int)(humidity);
// Der Payload wird vorbereitet

        unsigned char mydata[6];
        mydata[0] = batvalue;      
        mydata[1] = h & 0xFF; 
        mydata[2] = t >> 8;
        mydata[3] = t & 0xFF;
        mydata[4] = p >> 8;
        mydata[5] = p & 0xFF;  
           
// Die Daten werden übertragen
// Wichtig ist an dieser Stelle das der Port 2 genutzt wird
            
        LMIC_setTxData2(2, mydata, sizeof(mydata), 0);
        debugPrintLn(F("PQ")); //Packet queued
    }
    // Next TX is scheduled after TX_COMPLETE event.
}

/********************************************************************************    
 *     Verarbeite die empfangenen Daten
 ********************************************************************************/
void handle_received_message() {
        debugPrint(F("Empfangen="));
//        debugPrintLn(myrecdata[1]);
//        move_myservo(myrecdata[1]);
}
 /************************************************************************************+
  * Setup 
  * 
  * Hier werden die Parameter der LMIC eingestellt
  * Die Anmeldung des Node beim TTN
  * Die genutzte lokale Funk Frequenzen und die daraus resultierenden Kanäle
  * Aber auch die Datenrate und Sender Stärke (DR_SF7,14)
  * 
  */

#if !defined(OTAA)
void setup_abp() {
    // Set static session parameters. Instead of dynamically establishing a session
    // by joining the network, precomputed session parameters are be provided.
    #ifdef PROGMEM
    // On AVR, these values are stored in flash and only copied to RAM
    // once. Copy them to a temporary buffer here, LMIC_setSession will
    // copy them into a buffer of its own again.
    uint8_t appskey[sizeof(APPSKEY)];
    uint8_t nwkskey[sizeof(NWKSKEY)];
    memcpy_P(appskey, APPSKEY, sizeof(APPSKEY));
    memcpy_P(nwkskey, NWKSKEY, sizeof(NWKSKEY));
    LMIC_setSession (0x1, DEVADDR, nwkskey, appskey);
    #else
    // If not running an AVR with PROGMEM, just use the arrays directly
    LMIC_setSession (0x1, DEVADDR, NWKSKEY, APPSKEY);
    #endif

    #if defined(CFG_eu868)
    // Set up the channels used by the Things Network, which corresponds
    // to the defaults of most gateways. Without this, only three base
    // channels from the LoRaWAN specification are used, which certainly
    // works, so it is good for debugging, but can overload those
    // frequencies, so be sure to configure the full frequency range of
    // your network here (unless your network autoconfigures them).
    // Setting up channels should happen after LMIC_setSession, as that
    // configures the minimal channel set.
    // NA-US channels 0-71 are configured automatically
    LMIC_setupChannel(0, 868100000, DR_RANGE_MAP(DR_SF12, DR_SF7),  BAND_CENTI);      // g-band
    LMIC_setupChannel(1, 868300000, DR_RANGE_MAP(DR_SF12, DR_SF7B), BAND_CENTI);      // g-band
    LMIC_setupChannel(2, 868500000, DR_RANGE_MAP(DR_SF12, DR_SF7),  BAND_CENTI);      // g-band
    LMIC_setupChannel(3, 867100000, DR_RANGE_MAP(DR_SF12, DR_SF7),  BAND_CENTI);      // g-band
    LMIC_setupChannel(4, 867300000, DR_RANGE_MAP(DR_SF12, DR_SF7),  BAND_CENTI);      // g-band
    LMIC_setupChannel(5, 867500000, DR_RANGE_MAP(DR_SF12, DR_SF7),  BAND_CENTI);      // g-band
    LMIC_setupChannel(6, 867700000, DR_RANGE_MAP(DR_SF12, DR_SF7),  BAND_CENTI);      // g-band
    LMIC_setupChannel(7, 867900000, DR_RANGE_MAP(DR_SF12, DR_SF7),  BAND_CENTI);      // g-band
    LMIC_setupChannel(8, 868800000, DR_RANGE_MAP(DR_FSK,  DR_FSK),  BAND_MILLI);      // g2-band


    // For single channel gateways: Restrict to channel 0 when defined above
#ifdef CHANNEL0
    LMIC_disableChannel(1);
    LMIC_disableChannel(2);
    LMIC_disableChannel(3);
    LMIC_disableChannel(4);
    LMIC_disableChannel(5);
    LMIC_disableChannel(6);
    LMIC_disableChannel(7);
    LMIC_disableChannel(8);
#endif

    // TTN defines an additional channel at 869.525Mhz using SF9 for class B
    // devices' ping slots. LMIC does not have an easy way to define set this
    // frequency and support for class B is spotty and untested, so this
    // frequency is not configured here.
    #elif defined(CFG_us915)
    // NA-US channels 0-71 are configured automatically
    // but only one group of 8 should (a subband) should be active
    // TTN recommends the second sub band, 1 in a zero based count.
    // https://github.com/TheThingsNetwork/gateway-conf/blob/master/US-global_conf.json
    LMIC_selectSubBand(1);
    #endif

    // Disable link check validation
    LMIC_setLinkCheckMode(0);

    // TTN uses SF9 for its RX2 window.
    LMIC.dn2Dr = DR_SF9;

    // Set data rate and transmit power for uplink (note: txpow seems to be ignored by the library)
    LMIC_setDrTxpow(DR_SF7,14);   
}
#endif
  
void setup() {
    Serial.begin(115200);
    debugPrintLn(F("Starting"));
    // Init BME208
    if (!bme.begin()) 
    {  
        debugPrintLn(F("No valid bme280 sensor!"));
        delay(1000);  // allow serial to send.
        while (1) 
        {
           LowPower.powerDown(SLEEP_8S, ADC_OFF, BOD_OFF);   
        }

        switch(bme.chipModel())
        {
          case BME280::ChipModel_BME280:
            Serial.println("Found BME280 sensor! Success.");
            break;
          case BME280::ChipModel_BMP280:
            Serial.println("Found BMP280 sensor! No Humidity available.");
            break;
          default:
            Serial.println("Found UNKNOWN sensor! Error!");
        }
    }          
    // LMIC init
    os_init();
    // Reset the MAC state. Session and pending data transfers will be discarded.
    LMIC_reset();
    // Let LMIC compensate for +/- 1% clock error
    LMIC_setClockError(MAX_CLOCK_ERROR * 10 / 100);

/*******************************************************************    
 *     Unterschied zu OTAA muss bei ABP die Session definiert werden.
 *     Unter OTAA wird es von der LMIC erledigt
 ********************************************************************/  
//    delay(1000);  // allow serial to send.    
#if !defined(OTAA) 
    setup_apb();
#endif
    // Start job (sending automatically starts OTAA too)
    do_send(&sendjob);
    debugPrintLn(F("S")); // Setup complete!"

   
/****************************************************************/          

}
/**************************************************************************************
 * loop
 * Hier nun der Loop, also das Hauptprogramm
 * 
 */

void loop() 
{ 
    os_runloop_once();
}
