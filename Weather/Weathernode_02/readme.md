Weather Node 02
Sensortyp BMP280
Zusätzlich wird die Akku Spannung übertragen so das man
damit einen Trigger auslösen kann um z.B. eine SMS oder
Mail via IFTTT zu erhalten.
Gebaut für die Schulung des Nucleon e.V.
Hardware:

Arduino Pro-Mini 3.3V
RFM95W
BMP280 sensor.

Software:

LMIC https://github.com/matthijskooijman/arduino-lmic

LowPower library https://github.com/rocketscream/Low-Power

special adcvcc library from Charles (see : https://www.thethingsnetwork.org/forum/t/full-arduino-mini-lorawan-and-1-3ua-sleep-mode/8059/32?u=lex_ph2lb )

For licenses of the used libraries, check the links above.

# Anpassungen
29.04.2019 die Nodes können nun auch mittels OTAA betrieben werden.
           Der Sleep Mode ist angepasst worden.
           Zur weiteren Optimierung kann der Spannungswandler ausgebaut werden, dann sollte der BaseNode nur noch 0,008 mA in der Ruhephase ver$

